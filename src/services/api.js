let baseURL = 'http://localhost:8800'
export default {
  get (endpoint, token = null) {
    let headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    if (token) {
      headers['Authorization'] = 'JWT ' + token
    }
    return fetch(baseURL + endpoint, { method: 'GET', headers })
  }
}