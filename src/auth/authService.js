import auth0 from 'auth0-js'
import EventEmitter from 'events'
import authConfig from '../../auth_config.json'

const localStorageKey = 'loggedIn'
const loginEvent = 'loginEvent'

const webAuth = new auth0.WebAuth({
  domain: authConfig.domain,
  audience: `https://${authConfig.domain}/api/v2/`,
  redirectUri: `${window.location.origin}/`,
  clientID: authConfig.clientId,
  responseType: 'id_token token',
  scope: 'openid profile email  update:current_user_metadata update:users read:current_user'
})

// const webAuthManage = new auth0.WebAuth({
//   domain: authConfig.domain,
//   token: this.accessToken
// })

class AuthService extends EventEmitter {
  idToken = null
  profile = null
  accessToken = null
  tokenExpiry = null
  // Starts the user login flow
  login (customState) {
    webAuth.authorize({
      appState: customState
    })
  }
  // Handles the callback request from Auth0
  handleAuthentication () {
    return new Promise((resolve, reject) => {
      webAuth.parseHash((err, authResult) => {
        if (err) {
          reject(err)
        } else {
          this.localLogin(authResult)
          resolve(authResult.idToken)
        }
      })
    }).catch(console.error)
  }

  localLogin (authResult) {
    this.idToken = authResult.idToken
    this.profile = authResult.idTokenPayload
    this.accessToken = authResult.accessToken
    // Convert the JWT expiry time from seconds to milliseconds
    this.tokenExpiry = new Date(this.profile.exp * 1000)

    localStorage.setItem(localStorageKey, 'true')

    this.emit(loginEvent, {
      loggedIn: true,
      profile: authResult.idTokenPayload,
      state: authResult.appState || {},
      accessToken: authResult.accessToken
    })
  }
  /* eslint-disable */
  renewTokens () {
    return new Promise((resolve, reject) => {
      if (localStorage.getItem(localStorageKey) !== 'true') {
        reject('Not logged in')
      }

      webAuth.checkSession({}, (err, authResult) => {
        if (err) {
          reject(err)
        } else {
          this.localLogin(authResult)
          resolve(authResult)
        }
      })
    }).catch(console.error)
  }

  logOut () {
    localStorage.removeItem(localStorageKey)
    this.idToken = null
    this.tokenExpiry = null
    this.profile = null
    this.accessToken = null

    webAuth.logout({
      returnTo: window.location.origin
    })
    this.emit(loginEvent, { loggedIn: false })
  }

  isAuthenticated () {
    return (
      Date.now() < this.tokenExpiry &&
      localStorage.getItem(localStorageKey) === 'true'
    )
  }

  updateUserMetadata (data) {
    let self = this
    fetch(`https://${authConfig.domain}/api/v2/users/${this.profile.sub}`, {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'authorization': `Bearer ${this.accessToken}`
      },
      body: JSON.stringify({user_metadata: data})
    }).then((r) => r.json()).then((j) => {
      self.profile = j
      console.log(j)
    }).catch(console.error)
  }
}

export default new AuthService()
