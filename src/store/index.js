import Vue from 'vue'
import Vuex from 'vuex'
import authConfig from '../../auth_config.json'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    menuClick: 'realtime',
    userMetadata: null,
    profile: null,
    dataLength: 1
  },
  getters: {
    menuClick: state => state.menuClick,
    userMetadata: state => state.userMetadata,
    profile: state => state.profile,
    accessToken: state => state.accessToken,
    dataLength: state => state.dataLength

  },
  mutations: {
    setMenuClick: (state, menuClick) => {
      state.menuClick = menuClick
    },
    setUserMetadata: (state, userMetadata) => {
      state.userMetadata = userMetadata
    },
    setProfile: (state, profile) => {
      state.profile = profile
    },
    setAccessToken: (state, accessToken) => {
      state.accessToken = accessToken
    },
    setDataLength: (state, dataLength) => {
      state.dataLength = dataLength
    }
  },
  actions: {
    fetchUserMetadata (context, data) {
      context.commit('setProfile', data.profile)
      context.commit('setAccessToken', data.accessToken)
      fetch(`https://${authConfig.domain}/api/v2/users/${data.profile.sub}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'authorization': `Bearer ${data.accessToken}`
        }
      }).then((r) => r.json()).then((j) => {
        // console.log(r.status)
        context.commit('setUserMetadata', j)
        // console.log(j.user_metadata)
        if (j.user_metadata.application && j.user_metadata.feed) {
          context.commit('setMenuClick', 'realtime')
        } else {
          context.commit('setMenuClick', 'setting')
        }
      }).catch(console.error)
    },
    updateUserMetadata (context, data) {
      fetch(`https://${authConfig.domain}/api/v2/users/${context.state.profile.sub}`, {
        method: 'PATCH',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'authorization': `Bearer ${context.state.accessToken}`
        },
        body: JSON.stringify({user_metadata: data})
      }).then((r) => r.json()).then((j) => {
        context.commit('setUserMetadata', j)
      }).catch(console.error)
    }
  }
})
export default store
