import { Line, mixins } from 'vue-chartjs'
import 'chartjs-plugin-streaming'
import 'chartjs-plugin-zoom'
const { reactiveProp } = mixins

export default {
  extends: Line,
  // mixins: [mixins.reactiveProp],
  mixins: [reactiveProp],
  props: ['options'],
  mounted () {
    this.renderChart(this.chartData, this.options)
  }
}
