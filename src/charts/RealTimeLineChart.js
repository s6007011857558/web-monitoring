import { Line, mixins } from 'vue-chartjs'
import 'chartjs-plugin-streaming'
// import 'chartjs-plugin-zoom'
const { reactiveProp } = mixins

export default {
  extends: Line,
  mixins: [reactiveProp],
  props: {
    chartData: {
      type: Object,
      default: {}
    }
  },
  // props: ['chartData'],
  data: () => ({
    chartWrite: this.chartData,
    options: {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: 'Monitoring Feed'
      },
      scales: {
        xAxes: [{
          type: 'realtime',
          realtime: {
            duration: 60000,
            ttl: undefined,
            delay: 2000,
            pause: false
          },
          time: {
            // unit: 'second',
            // unitStepSize: 5,
            // round: 'second',
            // tooltipFormat: 'D MMM YY h:mm:ss A',
            displayFormats: {
              'second': 'h:mm:ss A'
            }
          }
        }],
        yAxes: [{
          type: 'linear',
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Value'
          },
          ticks: {
            suggestedMAX: 100,
            suggestedMin: 10,
            stepSize: 5
          }
        }]
      },
      tooltips: {
        mode: 'nearest',
        intersect: false
      },
      hover: {
        mode: 'nearest',
        intersect: false
      },
      plugins: {
        streaming: {
          frameRate: 60
        }
      }
    }
  }),
  mounted () {
    this.renderChart(this.chartWrite, this.options)
  },
  watch: {
    chartData () {
      this.$data._chart.update()
      // this.renderChart(this.chartData, this.options)
    }
  }
}
