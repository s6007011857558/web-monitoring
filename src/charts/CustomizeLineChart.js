import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
  extends: Line,
  // mixins: [mixins.reactiveProp],
  mixins: [reactiveProp],
  props: ['chartData', 'chartDataTest', 'options'],
  mounted () {
    this.renderChart(this.chartData, this.options)
  },
  watch: {
    chartData () {
      this.$data._chart.update()
      // this.renderChart(this.chartData, this.options)
    }
  }
}
