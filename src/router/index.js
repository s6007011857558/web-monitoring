import Vue from 'vue'
import Router from 'vue-router'
import Callback from '@/components/Callback'
import DashBoard from '@/pages/DashboardNew'
import auth from '@/auth/authService'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
    path: '/callback',
    name: 'callback',
    component: Callback
  },
  {
    path: '/',
    name: 'RealTime',
    component: DashBoard
  },
  {
    path: '/dashboard/history',
    name: 'History',
    component: DashBoard
  },
  {
    path: '/dashboard/report',
    name: 'Report',
    component: DashBoard
  },
  {
    path: '/profile',
    name: 'Profile',
    component: DashBoard
  },
  {
    path: '/notification',
    name: 'Notification',
    component: DashBoard
  },
  {
    path: '/setting',
    name: 'Setting',
    component: DashBoard
  }]
})

router.beforeEach((to, from, next) => {
  if (to.path === '/' || auth.isAuthenticated()) {
    return next()
  }
  // Specify the current path as the customState parameter, meaning it
  // will be returned to the application after auth
  auth.login({ target: to.path })
})

export default router
